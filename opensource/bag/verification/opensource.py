# -*- coding: utf-8 -*-

"""This module handles exporting schematic/layout from Virtuoso.
"""

from typing import TYPE_CHECKING, Optional, Dict, Any

from bag.io import write_file, open_temp

import os
from abc import ABC

if TYPE_CHECKING:
    from bag.verification.base import ProcInfo


# @@@ Note: this class gets mixed-in in SubProcessChecker, hence attributes and
# methods are provided through that path
class OpenSourceChecker(ABC):
    """the base Checker class for Open Source Tools.

    This class implement layout/schematic export procedures.

    Parameters
    ----------
    tmp_dir : str
        temporary file directory.
    max_workers : int
        maximum number of parallel processes.
    cancel_timeout : float
        timeout for cancelling a subprocess.
    source_added_file : str
        file to include for schematic export.
    """

    def __init__(self, interface):
        self._interface = interface

    def setup_export_layout(self, lib_name, cell_name, out_file, view_name='layout', params=None):
        # type: (str, str, str, str, Optional[Dict[str, Any]]) -> ProcInfo

        gds_file = os.path.join(self._interface.default_lib_path, lib_name + ".gds")
        out_file = os.path.abspath(out_file)

        run_dir = os.path.dirname(out_file)
        out_name = os.path.basename(out_file)
        log_file = os.path.join(run_dir, 'layout_export.log')

        os.makedirs(run_dir, exist_ok=True)

        cmd = ['cp', gds_file, out_file]
        return cmd, log_file, None, os.environ['BAG_WORK_DIR']

    def setup_export_schematic(self, lib_name, cell_name, out_file, view_name='schematic',
                               params=None):

        cwd = self._interface.xschem_project_dir()

        out_file = os.path.abspath(out_file)
        out_dir = os.path.dirname(out_file)
        out_filename = os.path.basename(out_file)

        sch_file = os.path.join(self._interface.default_lib_path, lib_name, cell_name, cell_name + ".sch")

        log_file = os.path.join(out_dir, 'schematic_export.log')

        # run command
        cmd = ['cd', cwd, '&&',
               'xschem', '-x',   # no X11
                         '-q',   # batch mode
                         '-n',   # netlist mode
                         '-o',   out_dir,
                         '--netlist_filename', out_filename,
                         sch_file ]

        return cmd, log_file, None, os.environ['BAG_WORK_DIR']
