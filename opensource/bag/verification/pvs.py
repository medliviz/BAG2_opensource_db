
# TODO: comments etc

from .opensource import OpenSourceChecker
import bag.verification.pvs

# NOTE: the first class provides the actual implmentations
# TODO: need to check if that is guaranteed by the MRO
class PVS(OpenSourceChecker, bag.verification.pvs.PVS):

  def __init__(self, *args, **kw_args):
    OpenSourceChecker.__init__(self, kw_args['interface'])
    bag.verification.pvs.PVS.__init__(self, *args, **kw_args)
